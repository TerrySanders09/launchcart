
import org.junit.Test;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Item;
import static org.junit.Assert.*;

public class testCart {

    @Test
    public void testCartTotal() {
        Cart cart = new Cart();

        cart.addItem(new Item("Headphones", 20));
        cart.addItem(new Item("sneakers", 100));
        assertEquals("120.0",Double.toString(cart.computeTotal()));

    };


}


